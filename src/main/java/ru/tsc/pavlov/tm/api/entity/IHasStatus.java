package ru.tsc.pavlov.tm.api.entity;

import ru.tsc.pavlov.tm.enumerated.Status;

public interface IHasStatus {

    Status getStatus();

    void setStatus(Status status);

}
