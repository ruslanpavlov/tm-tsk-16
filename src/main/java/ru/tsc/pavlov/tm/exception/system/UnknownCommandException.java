package ru.tsc.pavlov.tm.exception.system;

import ru.tsc.pavlov.tm.constant.TerminalConst;
import ru.tsc.pavlov.tm.exception.AbstractException;

public class UnknownCommandException extends AbstractException {

    public UnknownCommandException(final String value) {
        super("Error. Unknown command " + value + " Please use " + TerminalConst.HELP);
    }

    public UnknownCommandException() {
        super("Error. Please enter command. Please use " + TerminalConst.HELP);
    }

}
